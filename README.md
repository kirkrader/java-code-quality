Copyright &copy; 2017 Kirk Rader. All rights reserved.

# Code Quality Checks

Common code style and static analysis configuration rules used for all Java-based projects.

This repository contains four files, packaged in a Maven-friendly format:

- _checkstyle.xml_

- _findbugs-include-filter.xml_

- _findbugs-exclude-filter.xml_

The contents of _checkstyle.xml_ are (mostly) based on <https://google.github.io/styleguide/javaguide.html>.

## Breaking the CI Build

We have a "zero tolerance" policy toward technical debt for all _new_ projects. This means that whenever you are creating a new POM file you must  add a dependency to this project and invoke the Checkstyle and FindBugs plugins in your Maven POM files in such a way as to "break the build:"

```xml
...

<properties>
  ...
  <javaCodeQuality.version>1.11-SNAPSHOT</javaCodeQuality.version>
  <checkstyle.tools.version>8.7</checkstyle.tools.version>
  <findsecbugs.version>1.6.0</findsecbugs.version>
  ...
</properties>

...

<build>

  <plugins>

    ...

    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-checkstyle-plugin</artifactId>
      <version>2.17</version>
      <dependencies>
      	<dependency>
      	  <groupId>com.puppycrawl.tools</groupId>
      	  <artifactId>checkstyle</artifactId>
      	  <version>${checkstyle.tools.version}</version>
      	</dependency>
      	<dependency>
      	  <groupId>us.rader.swgov</groupId>
      	  <artifactId>java-code-quality</artifactId>
      	  <version>${javaCodeQuality.version}</version>
      	</dependency>
      </dependencies>
      <executions>
      	<execution>
      	  <phase>validate</phase>
      	  <goals>
      	    <goal>check</goal>
      	  </goals>
      	</execution>
      </executions>
      <configuration>
      	<configLocation>rader/checkstyle.xml</configLocation>
        <includeTestSourceDirectory>true</includeTestSourceDirectory>
      	<logViolationsToConsole>true</logViolationsToConsole>
      	<failOnViolation>true</failOnViolation>
      	<violationSeverity>info</violationSeverity>
      </configuration>
    </plugin>

    <plugin>
      <groupId>org.codehaus.mojo</groupId>
      <artifactId>findbugs-maven-plugin</artifactId>
      <version>3.0.4</version>
      <dependencies>
      	<dependency>
      	  <groupId>us.rader.swgov</groupId>
      	  <artifactId>java-code-quality</artifactId>
      	  <version>${javaCodeQuality.version}</version>
      	</dependency>
      </dependencies>
      <configuration>
      	<effort>Max</effort>
      	<threshold>Low</threshold>
      	<xmlOutput>true</xmlOutput>
      	<excludeFilterFile>rader/findbugs-exclude-filter.xml</excludeFilterFile>
      	<includeFilterFile>rader/findbugs-include-filter.xml</includeFilterFile>
      	<plugins>
      	  <plugin>
      	    <groupId>com.h3xstream.findsecbugs</groupId>
      	    <artifactId>findsecbugs-plugin</artifactId>
      	    <version>${findsecbugs.version}</version>
      	  </plugin>
      	</plugins>
      </configuration>
      <executions>
      	<execution>
      	  <phase>verify</phase>
      	  <goals>
      	    <goal>check</goal>
      	  </goals>
      	</execution>
      </executions>
    </plugin>

    ...

  </plugins>

</build>
```

## Generating Violations Reports

For existing projects, we will enforce adherence to these guidelines gradually over time. Each POM file must be altered to use this module in conjunction with the `checkstyle:checkstyle`  and `findbugs:findbugs` Maven goals to generate violations reports as part of their build output.

1. Using this technique, scrum masters and development team leads are responsible for assigning tasks to reduce technical debt over time.

2. Once all technical debt has been addressed in a given project, its POM files must be altered to conform to the guidelines in [Breaking the CI Build](#breaking).

To accomplish this, the Checkstyle and FindBugs maven plugins must be configured in both the `<build>` and `<reporting>` sections of your poject's POM file. Begin by declaring properties to use in multiple sections:

```xml
<properties>
  ...
  <javaCodeQuality.version>1.11-SNAPSHOT</javaCodeQuality.version>
  <checkstyle.version>2.17</checkstyle.version>
  <checkstyle.tools.version>8.7</checkstyle>
  <findbugs.version>3.0.4</findbugs.version>
  <findsecbugs.version>1.6.0</findsecbugs.version>
  ...
</properties>
```

Next, configure the Checkstyle and FindBugs plugins' dependencies in the `<build>` section:

```xml
<build>

  ...

  <plugins>

    ...

    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-checkstyle-plugin</artifactId>
      <version>${checkstyle.version}</version>
      <dependencies>
        <dependency>
          <groupId>com.puppycrawl.tools</groupId>
          <artifactId>checkstyle</artifactId>
          <version>${checkstyle.tools.version}</version>
        </dependency>
        <dependency>
          <groupId>us.rader.swgov</groupId>
          <artifactId>java-code-quality</artifactId>
          <version>${javaCodeQuality.version}</version>
        </dependency>
      </dependencies>
    </plugin>

    <plugin>
      <groupId>org.codehaus.mojo</groupId>
      <artifactId>findbugs-maven-plugin</artifactId>
      <version>${findbugs.version}</version>
      <dependencies>
        <dependency>
          <groupId>us.rader.swgov</groupId>
          <artifactId>java-code-quality</artifactId>
          <version>${javaCodeQuality.version}</version>
        </dependency>
      </dependencies>
    </plugin>

    ...

  </plugins>

  ...

</build>
```

To generate the reports during the `site` phase you must configure the plugins in the `<reporting> <plugins> ... </plugins> </reporting>` section:

```xml
<reporting>

  ...

  <plugins>

    ...

    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-checkstyle-plugin</artifactId>
      <version>${checkstyle.version}</version>
      <reportSets>
        <reportSet>
          <reports>
            <report>checkstyle</report>
          </reports>
        </reportSet>
      </reportSets>
      <configuration>
        <configLocation>rader/checkstyle.xml</configLocation>
        <includeTestSourceDirectory>true</includeTestSourceDirectory>
        <logViolationsToConsole>true</logViolationsToConsole>
        <violationSeverity>info</violationSeverity>
        <encoding>UTF-8</encoding>
      </configuration>
    </plugin>

    <plugin>
      <groupId>org.codehaus.mojo</groupId>
      <artifactId>findbugs-maven-plugin</artifactId>
      <version>${findbugs.version}</version>
      <configuration>
        <effort>Max</effort>
        <threshold>Low</threshold>
        <xmlOutput>true</xmlOutput>
        <excludeFilterFile>rader/findbugs-exclude-filter.xml</excludeFilterFile>
        <includeFilterFile>rader/findbugs-include-filter.xml</includeFilterFile>
        <plugins>
          <plugin>
            <groupId>com.h3xstream.findsecbugs</groupId>
            <artifactId>findsecbugs-plugin</artifactId>
            <version>${findsecbugs.version}</version>
          </plugin>
        </plugins>
      </configuration>
    </plugin>

    ...

  </plugins>

  ...

</reporting>
```

This "double" configuration is necessary because the `<reporting>` section does not support specifying dependencies for individual plugins, as required in order to use this project and _findsecbugs-plugin_.

> See <https://maven.apache.org/plugins/maven-checkstyle-plugin/> and <http://gleclaire.github.io/findbugs-maven-plugin/> for more information on configuring the Checkstyle and FindBugs plugins in this way.

Use the `site` and `site:stage` goals to generate the code quality reports as part of the overall project build reports:

    mvn clean install site site:stage

If all goes well, you should then be able to view the Checkstyle and FindBugs output in the _Project Reports_ section when browsing _target/staging/index.html_.

### Integrated Development Environments

Developers are strongly encouraged to configure their IDE's to use the same rules as enforced by this module so as to avoid surprises at build time. Failing this, you can always just perform a manual build from the command-line before committing -- which is good practice in any event.

    mvn clean install -l output.txt

Remember to check _output.txt_ for errors and warnings as well as any reports generated by the Checkstyle and FindBugs Maven plugins.
